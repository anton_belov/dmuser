#!/usr/bin/env python
'''
dmuser - a DRUP-based trimming + MUS extraction pipeline.

Uses: glucose3.0, drup-trim, trace2pc, muser-2

(c) Anton Belov and Marijn Heule, 2013
'''

from __future__ import print_function
import argparse
import os
import oshelpers as osh
import re
import signal
import shutil
import sys
import threading

# helper to count clauses
def count_clauses(filename, trust_header=True):
    '''Computes the number of clauses in file_name; if trust_header is True,
    returns the number listed in p cnf line; otherwise gets the actual count
    @return number of clauses or -1 if couldn't figure it out
    '''
    (f, cnt) = (open(filename), -1)
    for line in f:
        if line.startswith("p cnf "):
            if trust_header: 
                cnt = int(line.split()[3])
                break
            cnt = 0
        if re.match("([-0-9]+\s+)*([-0-9]+\s*)", line): 
            if cnt == -1: cnt = 0
            cnt += 1
    f.close()
    return cnt
    
    
# main implementation class
class DMuser(object):
    '''Implementation of DRUP-based MUS extraction pipeline'''
    class Stats:
        '''To keep the stats'''
        def __init__(self):
            self.init_iter()
            self.trimming_time_acc = self.sat_time_acc = self.prepro_time_acc = None
            self.pc_time_acc = None
            self.mus_core_time_acc = self.mus_pc_time_acc = None
            self.mus_core_hmuc_time_acc = self.mus_trace_time_acc = None
            self.num_iter = 0
        def _str(self, value, suff=""):
            if value is None: return "n/a"
            if isinstance(value, int): fmt = "%d"
            elif isinstance(value, float): fmt = "%.2f"
            else: fmt ="%r"
            return (fmt+"%s") % (value, suff)
        def _zero(self, value): return 0 if value is None else value
        def _sum(self, tl):
            if all([x is None for x in tl]): return None
            else: return sum([self._zero(x) for x in tl])
        def init_iter(self):
            '''initialize per-iteration stats''' 
            self.input_size = self.core_size = None
            self.input_lemmas = self.core_lemmas = self.core_res = None
            self.trimming_time = self.sat_time = self.prepro_time = None
            self.pc_time = None
            self.mus_core_time = self.mus_pc_time = None
            self.mus_core_hmuc_time = self.mus_trace_time = None
            self.mus_nec_size = self.mus_unk_size = None
        def update_accs(self):
            '''call this to update total stats'''
            self.trimming_time_acc = self._sum([self.trimming_time_acc, self.trimming_time])
            self.sat_time_acc = self._sum([self.sat_time_acc, self.sat_time])
            self.prepro_time_acc = self._sum([self.prepro_time_acc, self.prepro_time])
            self.pc_time_acc = self._sum([self.pc_time_acc, self.pc_time])
            self.mus_core_time_acc = self._sum([self.mus_core_time_acc, self.mus_core_time])
            self.mus_pc_time_acc = self._sum([self.mus_pc_time_acc, self.mus_pc_time])
            self.mus_core_hmuc_time_acc = self._sum([self.mus_core_hmuc_time_acc, self.mus_core_hmuc_time])
            self.mus_trace_time_acc = self._sum([self.mus_trace_time_acc, self.mus_trace_time])
        def print_iter(self):
            '''prints per-interation stats'''
            total_trim_time = self._sum([self.trimming_time, self.pc_time, self.prepro_time, self.sat_time])
            total_mus_time = self._sum([self.mus_core_time, self.mus_pc_time, self.mus_core_hmuc_time, self.mus_trace_time])
            total_time = self._sum([total_trim_time, total_mus_time])
            mus_size = self._sum([self.mus_nec_size, self.mus_unk_size])
            print("c Iteration %d stats" % self.num_iter)
            print("c input clauses             : " + self._str(self.input_size))
            print("c   input lemmas            : " + self._str(self.input_lemmas))
            print("c core clauses              : " + self._str(self.core_size))
            print("c   core lemmas             : " + self._str(self.core_lemmas))
            print("c   core res.steps          : " + self._str(self.core_res))
            print("c MUS clauses               : " + self._str(mus_size))
            print("c   nec clauses             : " + self._str(self.mus_nec_size))
            print("c   unknown clauses         : " + self._str(self.mus_unk_size))
            print("c total trimming time       : " + self._str(total_trim_time, " sec"))
            print("c   drup-trim time          : " + self._str(self.trimming_time, " sec"))
            print("c   trace2pc time           : " + self._str(self.pc_time, " sec"))
            print("c   prepro time             : " + self._str(self.prepro_time, " sec"))
            print("c   SAT solving time        : " + self._str(self.sat_time, " sec"))
            print("c MUS extraction time       : " + self._str(total_mus_time, " sec"))
            print("c   MUS core, m-2 time      : " + self._str(self.mus_core_time, " sec"))
            print("c   MUS pc, m-2 time        : " + self._str(self.mus_pc_time, " sec"))
            print("c   MUS core, hmuc time     : " + self._str(self.mus_core_hmuc_time, " sec"))
            print("c   MUS trace, hmuc time    : " + self._str(self.mus_trace_time, " sec"))
            print("c Total time                : " + self._str(total_time, " sec"))
        def print_accs(self):
            '''prints the accumulated stats'''
            total_trim_time = self._sum([self.trimming_time_acc, self.pc_time_acc, self.prepro_time_acc, self.sat_time_acc])
            total_mus_time = self._sum([self.mus_core_time_acc, self.mus_pc_time_acc, self.mus_core_hmuc_time_acc, self.mus_trace_time_acc])
            total_time = self._sum([total_trim_time, total_mus_time])
            print("c Accumulated stats")
            print("c number of iterations      : %d" % self.num_iter)
            print("c total trimming time       : " + self._str(total_trim_time, " sec"))
            print("c   drup-trim time          : " + self._str(self.trimming_time_acc, " sec"))
            print("c   trace2pc time           : " + self._str(self.pc_time_acc, " sec"))
            print("c   prepro time             : " + self._str(self.prepro_time_acc, " sec"))
            print("c   SAT solving time        : " + self._str(self.sat_time_acc, " sec"))
            print("c MUS extraction time       : " + self._str(total_mus_time, " sec"))
            print("c   MUS core, m-2 time      : " + self._str(self.mus_core_time_acc, " sec"))
            print("c   MUS pc, m-2 time        : " + self._str(self.mus_pc_time_acc, " sec"))
            print("c   MUS core, hmuc time     : " + self._str(self.mus_core_hmuc_time_acc, " sec"))
            print("c   MUS trace, hmuc time    : " + self._str(self.mus_trace_time_acc, " sec"))
            print("c Total time                : " + self._str(total_time, " sec"))
    
    def __init__(self):
        self.verbosity = 1
        self.pre = "erun4"
        self.pre_once = False
        self.super_mode = False
        self.ve_iters = 5           # number of VE iterations
        self.ve_grow = "10*(2**a)"
        self.inputcnf_path = self.outputcnf_path = self.workdir_path = self.tools_path = None
        self.corecnf_path = self.fulldrup_path = self.fulltrace_path = self.fullpc_path = None
        self.compute_mus_core = self.compute_mus_core_hmuc = False
        self.compute_mus_trace = self.compute_mus_pc = False
        self.keep_files = False
        self.stats = DMuser.Stats()
        self.mus_time_lim = 0
        self.call_time_lim = 0
        self.adapt_time_lim = False 
        self.muser2_use_prog = False
        self.no_stats = False
        # to keep track of the results
        self.num_nec = self.num_unk = None
        self.nec_ids = self.unk_ids = None
        self.clauses = self.max_var = None
        self.nec_ids_path = None    # file to use to pass nec to extractors 

    def _report(self, level, msg):
        if self.verbosity >= level: 
            osh.print_status("dmuser", msg)
            sys.stdout.flush()

    def _update_time(self, old_time, start_time):
        t = osh.get_cpu_time() - start_time
        return t if old_time is None else old_time + t

    def _setup(self):
        '''Checks that all the required tools are present, and sets up al the 
        paths'''
        self.glucose_core_path = osh.getToolPath(self.tools_path, "glucose-core")
        self.glucose_simp_path = osh.getToolPath(self.tools_path, "glucose-simp")
        self.druptrim_path = osh.getToolPath(self.tools_path, "drup-trim")
        self.trace2pc_path = osh.getToolPath(self.tools_path, "trace2pc")
        if self.compute_mus_core or self.compute_mus_pc:
            self.muser2_path = osh.getToolPath(self.tools_path, "muser-2")
        if self.compute_mus_core_hmuc:
            self.haifa_muc_path = osh.getToolPath(self.tools_path, "haifa_muc")
        if self.compute_mus_trace:
            self.haifa_muc_trace_path = osh.getToolPath(self.tools_path, "haifa_muc_trace")
        self.workdir_path = osh.createWorkDir(self.workdir_path, self.keep_files)
        if self.fulldrup_path is None:
            self.fulldrup_path = os.path.join(self.workdir_path, "full.drup")
        if self.verbosity >= 2: # print all paths
            self._report(2, "paths: ")
            for n in sorted(self.__dict__.keys()):
                if n.endswith("_path"): self._report(2, "%s = %s" % (n, self.__dict__[n]))
            
    def _solve(self, input_cnf, output_drup=None):
        '''Runs glucose core on input_cnf with certificate to output_drup if the
        latter is not None; 
        @return the exit code of glucose (0, 10, 20)
        '''
        cmd = self.glucose_core_path + " " + input_cnf
        if (output_drup is not None):
            cmd += " -certified -certified-output=" + output_drup
        self._report(2, "running " + cmd)
        s_time = osh.get_cpu_time()
        rcode = osh.run_command(cmd, out=(None if self.verbosity <= 2 else sys.stdout))
        self.stats.sat_time = self._update_time(self.stats.sat_time, s_time)
        self._report(2, "finished with %r" % rcode)
        return rcode

    def _simplify(self, input_cnf, output_cnf, output_drup=None, grow=None):
        '''Runs glucose-simp -simp on input_cnf, writing the simplified instance
        to output_cnf, and the DRUP trace to output_drup, if the latter is not 
        None; if grow is not None, its passed to glucose
        @return the exit code of glucose (0, 10, 20)
        '''
        cmd = self.glucose_simp_path + " -cl-lim=-1 " + input_cnf + " -dimacs=" + output_cnf
        if (output_drup is not None): 
            cmd += " -certified -certified-output=" + output_drup
        if (grow is not None): cmd += " -grow=" + str(grow) 
        self._report(2, "running " + cmd)
        s_time = osh.get_cpu_time()
        rcode = osh.run_command(cmd, out=(None if self.verbosity <= 2 else sys.stdout))
        self.stats.prepro_time = self._update_time(self.stats.prepro_time, s_time)
        self._report(2, "finished with %r" % rcode)
        return rcode

    def _trim(self, input_cnf, input_drup, output_cnf, reduced_drup=None, output_trace=None):
        '''Runs drup-trim to generate a trimmed core of the input_cnf, based on
        the DRUP trace input_drup; result is written to output_cnf; if
        reduced_drup is not None, the reduced DRUP trace is written to it (-l option);
        if output_trace is not None, also computes the TRACECHECK trace and 
        writes it into output_trace
        @return exit code of drup-trim,
        '''
        cmd = self.druptrim_path + " " + input_cnf + " " + input_drup + " -c " + output_cnf
        if reduced_drup is not None:
            cmd += " -l " + reduced_drup
        if output_trace is not None:
            cmd += " -r " + output_trace
        self._report(2, "running " + cmd)
        s_time = osh.get_cpu_time()
        c = osh.start_command(cmd)
        for line in c[0]:
            r = re.match("c ([0-9]+) of ([0-9]+) clauses in core", line)
            if r is not None:
                self.stats.core_size = int(r.group(1))
            r = re.match("c ([0-9]+) of ([0-9]+) lemmas in core using ([0-9]+) resolution steps", line)
            if r is not None:
                self.stats.core_lemmas = int(r.group(1))
                self.stats.input_lemmas = int(r.group(2))
                self.stats.core_res = int(r.group(3))
            if self.verbosity >= 3: print(line, end="")
        rcode = osh.finish_command(c[1])
        self.stats.trimming_time = self._update_time(self.stats.trimming_time, s_time)
        self._report(2, "finished with %r" % rcode)
        if output_trace is not None: # sort the trace
            osh.sort(output_trace, num=True)
        return rcode

    def _run_muser(self, input_f, to=0, input_is_pc=False, do_unsat_chk=False, nid_file=None, call_to=0):
        '''Kicks off muser-2 on input formula. If input_is_pc is True, the
        input is assumed to be .pc file produced by trace2pc
        @param nid_file -- the file with neccessary IDs
        @param call_to -- if not 0, the timeout for a single call (secs)
        @return exit code of muser-2'''
        cmd = "%s -comp -v %d -T %d " % (self.muser2_path, self.verbosity, to)
        if input_is_pc: cmd += "-pc -ig0 "
        if do_unsat_chk: cmd += "-ichk "
        if self.muser2_use_prog: cmd += "-prog "
        if nid_file is not None: cmd += "-nidfile " + nid_file + " "
        cmd += input_f
        self._report(2, "running " + cmd)
        s_time = osh.get_cpu_time()
        c = osh.start_command(cmd)
        def interrupt_solver():
            self._report(2, "per-call timeout exceeeded, terminating hmuc") 
            c[1].send_signal(signal.SIGINT)
        timer = None
        for line in c[0]:
            r = re.match("c nec: ([0-9]+) unk: ([0-9]+)", line)
            if r is not None: # got the result line
                self.num_nec = int(r.group(1))
                self.num_unk = int(r.group(2))
            elif line.startswith("v "): # for the ids
                w = line.split()
                self.nec_ids = [int(i) for i in w[1:self.num_nec+1]]
                self.unk_ids = [int(i) for i in w[self.num_nec+1:-1]]
            else: 
                if re.match("c \[[.0-9]+ sec\] wrkr-0", line) is not None:
                    if call_to != 0:
                        if timer is not None:
                            timer.cancel()
                            timer = None
                        timer = threading.Timer(call_to, interrupt_solver) 
                        timer.start()
                if self.verbosity >= 2: 
                    print(line, end="")
                    sys.stdout.flush()
        rcode = osh.finish_command(c[1])
        if timer is not None: timer.cancel()
        if input_is_pc:
            self.stats.mus_pc_time = self._update_time(self.stats.mus_pc_time, s_time)
        else:
            self.stats.mus_core_time = self._update_time(self.stats.mus_core_time, s_time)
        self._report(2, "finished with %r" % rcode)
        return rcode

    def _run_hmuc(self, input_f, to=0, input_is_trace=False, nid_file=None, call_to=0):
        '''Kicks off either haifa_muc or haifa_muc_trace on input formula.
        @param nid_file -- the file with neccessary IDs
        @param call_to -- if not 0, the timeout for a single call (secs)
        @return exit code of haifa_muc'''
        cmd = "%s " % (self.haifa_muc_trace_path if input_is_trace else self.haifa_muc_path)
        if to: cmd += (" -cpu-lim=%d " % to)
        if nid_file is not None: cmd += (" -nidfile=%s " % nid_file)
        cmd += input_f
        self._report(2, "running " + cmd)
        s_time = osh.get_cpu_time()
        c = osh.start_command(cmd)
        def interrupt_solver():
            self._report(2, "per-call timeout exceeeded, terminating hmuc") 
            c[1].send_signal(signal.SIGINT)
        timer = None
        for line in c[0]:
            r = re.match("Summary: [0-9]+ ([0-9]+) ([0-9]+)", line)
            if r is not None: # got the result line
                self.num_nec = int(r.group(1))
                self.num_unk = int(r.group(2))
                if self.verbosity >= 2:
                    print("[haifa_muc] " + line, end="")
                    sys.stdout.flush()
            elif line.startswith("v "): # for the ids
                w = line.split()
                self.nec_ids = [int(i) for i in w[1:self.num_nec+1]]
                self.unk_ids = [int(i) for i in w[self.num_nec+1:-1]]
            elif re.match("c iter [0-9]+", line) is not None:
                if call_to != 0:
                    if timer is not None:
                        timer.cancel()
                        timer = None
                    timer = threading.Timer(call_to, interrupt_solver) 
                    timer.start()
                if self.verbosity >= 3: 
                    print(line, end="")
                    sys.stdout.flush()
        rcode = osh.finish_command(c[1])
        if timer is not None: timer.cancel()
        if input_is_trace:
            self.stats.mus_trace_time = self._update_time(self.stats.mus_trace_time, s_time)
        else:
            self.stats.mus_core_hmuc_time = self._update_time(self.stats.mus_core_hmuc_time, s_time)
        self._report(2, "finished with %r" % rcode)
        return rcode

    def _compute_mus(self, input_f, to=0, use_muser=True, input_is_trace=False, nid_file=None, call_to=0):
        '''Kicks off either muser-2 or haifa_muc on input formula.
        @param nid_file - if not None, a path to file with necessary IDs
        @param call_to -- if not 0, the timeout for a single call (secs)
        @return exit code of the tool'''
        if use_muser:
            rcode = self._run_muser(input_f, to, input_is_trace, nid_file=nid_file, call_to=call_to)
        else:
            rcode = self._run_hmuc(input_f, to, input_is_trace, nid_file=nid_file, call_to=call_to)
        self.stats.mus_nec_size = self.num_nec
        self.stats.mus_unk_size = self.num_unk
        if rcode == 0 or rcode == 20:
            self._report(2, "MUS (approximation): %d necessary clauses, %d unknown" % (self.num_nec, self.num_unk))
        return rcode

    def _compute_pc(self, input_trace, output_pc):
        '''Runs trace2pc on the specified input_trace file; writes output to
        output_pc
        @return exit code of trace2pc
        '''
        cmd = self.trace2pc_path + " " + input_trace
        self._report(2, "running " + cmd)
        s_time = osh.get_cpu_time()
        rcode = osh.run_command(cmd, out=open(output_pc, "w"))
        self.stats.pc_time = self._update_time(self.stats.pc_time, s_time)
        self._report(2, "finished with %r" % rcode)
        return rcode

    # functions that extract the top-level clauses
    
    def _get_clauses_cnf(self, input_f):
        '''Assuming the input_f is a path to file in CNF, gets the clauses, and
        returns ([clauses], max_var) or (None, None) in case of error. clauses[id] 
        is the clause with ID id'''
        try:
            (clauses, max_var) = ([None], 0)
            for line in open(input_f):
                if line.startswith("p ") or line.startswith("c "): continue
                clauses.append(line.strip())
                for lit in line.strip().split():
                    var = abs(int(lit))
                    if (var > max_var): max_var = var
            return (clauses, max_var)
        except IOError:
            return (None, None)

    def _get_clauses_pc(self, input_f):
        '''Assuming the input_f is a path to file in .pc format, gets the clauses
        (as opposed to lemmas), and returns ([clauses], max_var) or (None, None) 
        in case of error. clauses[id] is the clause with ID id'''
        try:
            (clauses, max_var) = ([None], 0)
            for line in open(input_f):
                if line.startswith("c "): continue
                w = line.strip().split()
                if line.startswith("p "):
                    num_cls = int(w[3])
                    continue
                cls_str = ""
                for lit in w[:-2]:
                    cls_str += lit + " "
                    var = abs(int(lit))
                    if (var > max_var): max_var = var
                cls_str += "0"
                clauses.append(cls_str)
                num_cls -= 1
                if num_cls == 0: break # no more "real" clauses
            return (clauses, max_var)
        except IOError:
            return (None, None)

    def _get_clauses_trace(self, input_f):
        '''Assuming the input_f is a path to file in TRACECHECK format, gets the 
        clauses (as opposed to lemmas), and returns ([clauses], max_var) or 
        (None, None) in case of error. clauses[id] is the clause with ID id'''
        try:
            (clauses, max_var) = ([None], 0)
            for line in open(input_f):
                if line.startswith("c "): continue
                # original clasuses are in the form ID clause 0 0
                r = re.match("([0-9]+\s+)(([-0-9]+\s+)*0)(\s+)0", line) 
                if r is not None:
                    id = int(r.group(1))
                    cls_str = r.group(2)
                    if id > len(clauses) - 1:
                        clauses.extend([None for x in range(len(clauses), id + 1)])
                    clauses[id] = cls_str
                    for lit in cls_str.split():
                        var = abs(int(lit))
                        if (var > max_var): max_var = var
            return (clauses, max_var)
        except IOError:
            return (None, None)
    
    def _write_output(self, output_cnf):
        '''Writes out output CNF file into output_cnf
        @return True on success, False on failure
        '''
        assert self.nec_ids is not None and self.unk_ids is not None, "should have the necessary and unknown IDs available by now"
        if self.compute_mus_core or self.compute_mus_core_hmuc:
            (self.clauses, self.max_var) = self._get_clauses_cnf(self.corecnf_path)
        elif self.compute_mus_pc:
            (self.clauses, self.max_var) = self._get_clauses_pc(self.fullpc_path)
        elif self.compute_mus_trace:
            (self.clauses, self.max_var) = self._get_clauses_trace(self.fulltrace_path)
        else:
            return False
        if self.clauses is None: return False
        outf = open(output_cnf, "w")
        print("c generated by dmuser", file=outf)
        print("c nec=%d unk=%d" % (self.num_nec, self.num_unk), file=outf)
        print("p cnf %d %d" % (self.max_var, self.num_nec + self.num_unk), file=outf)
        for idx in self.nec_ids: print(self.clauses[idx], file=outf)
        for idx in self.unk_ids: print(self.clauses[idx], file=outf)
        outf.close()
        return True

    def _get_neccessary_clause_ids(self):
        '''Given the current set of necessary clauses, specified using self.clauses 
        and self.nec_ids, figures out the IDs of the necessary clauses in the
        .cnf/.trace/.pc files produced after trimming.
        @return the list of IDs or None if could not be computed
        '''
        def canonize(cls_str):
            '''Returns a canonical representation of the clause'''
            l = sorted([int(x) for x in cls_str.split() if x != "0"])
            return reduce(lambda x, y: str(x)+" "+str(y), l)
        
        if self.clauses is None: return None # first time
        assert self.nec_ids is not None, "should have the necessary IDs available by now"
        
        nec_cls = set()
        for idx in self.nec_ids: nec_cls.add(canonize(self.clauses[idx]))
        
        # get the list of clauses
        if self.compute_mus_core or self.compute_mus_core_hmuc:
            clauses = self._get_clauses_cnf(self.corecnf_path)[0]
        elif self.compute_mus_pc:
            clauses = self._get_clauses_pc(self.fullpc_path)[0]
        elif self.compute_mus_trace:
            clauses = self._get_clauses_trace(self.fulltrace_path)[0]
        else:
            return None

        nec_ids = []
        for idx, cls_str in enumerate(clauses):
            if cls_str is not None and canonize(cls_str) in nec_cls: nec_ids.append(idx)
        return nec_ids
        
    #
    # different preprocessing flows; all have the same arguments which represent
    # file paths; 
    #
    
    def pre_none(self, input_cnf, full_drup):
        '''No-preprocessing'''
        self._report(1, "refuting %s" % input_cnf)
        rcode = self._solve(input_cnf, full_drup)
        if rcode != 20:
            self._report(0, "ERROR: got %d trying to refute %s" % (rcode, input_cnf))
            return False
        return True
        
    def pre_erun1(self, input_cnf, full_drup, adaptive=False, use_reduced_drup=False):
        '''Standard loop; if adaptive is True, breaks out when the number of 
        conflicts during solving grows
        '''
        assert not os.path.exists(full_drup), "full_drup file should not exist here"
        self._report(2, "starting erun1 on %s, writing DRUP to %s" % (input_cnf, full_drup))
        # initialize all the work files ...
        work_cnf = os.path.join(self.workdir_path, "work.cnf")
        shutil.copy(input_cnf, work_cnf)
        self._report(2, "initialized %s with %s" % (work_cnf, input_cnf))
        simp_cnf = os.path.join(self.workdir_path, "simp.cnf")
        simp_drup = os.path.join(self.workdir_path, "simp.drup")
        refutation_drup = os.path.join(self.workdir_path, "refutation.drup")
        reduced_drup = os.path.join(self.workdir_path, "reduced.drup") if use_reduced_drup else None
        refutation_confls = sys.maxint
        unsat_simp = False # will be True, if got UNSAT during simplification
        for a in range(0,self.ve_iters):
            self._report(1, "simp: simplifying %s" % work_cnf)
            if self.verbosity >= 1: 
                self._report(1, "simp:     clauses before simp: %d" % count_clauses(work_cnf))
            rcode = self._simplify(work_cnf, simp_cnf, simp_drup, grow=eval(self.ve_grow))
            if rcode == 10:
                self._report(0, "ERROR: got rcode %d (SATISFIABLE) during simplification" % rcode)
                return False
            osh.cat(full_drup, [simp_drup], append=True)
            if rcode == 20:
                self._report(1, "got UNSATISFIABLE during simplification, breaking out")
                unsat_simp = True
                break
            if self.verbosity >= 1: 
                self._report(1, "simp:     clauses after simp: %d" % count_clauses(simp_cnf))
            rcode = self._solve(simp_cnf, refutation_drup)
            if rcode != 20:
                self._report(0, "ERROR: got rcode %d (SATISFIABLE) during simplification" % rcode)
                return False
            rcode = self._trim(simp_cnf, refutation_drup, work_cnf, reduced_drup)
            if  rcode != 0: 
                self._report(0, "ERROR: got rcode %d during trimming" % rcode)
                return False
            if adaptive:
                confls = count_clauses(refutation_drup, trust_header=False)
                self._report(2, "conflict count: %d, previous: %d" % (confls, refutation_confls))
                if confls > refutation_confls: 
                    self._report(2, "conflict count increased, aborting simplification")
                    break
                refutation_confls = confls
        # assemble the final proof
        if not unsat_simp:
            osh.cat(full_drup, [reduced_drup if reduced_drup is not None else refutation_drup], append=True)
        return True

    def single_trim(self, input_cnf, core_cnf, full_drup, full_trace=None):
        '''Preprocess, then trim (compute core); if full_trace is not None, also
        store the proof '''

        # get the full DRUP trace into self.fulldrup_path
        rval = None
        if self.pre == "none":
            rval = self.pre_none(input_cnf, full_drup)
        elif self.pre == "erun1":
            rval = self.pre_erun1(input_cnf, full_drup)
        elif self.pre in ["erun3", "erun4", "erun41", "erun5", "erun6"]:
            self.ve_iters = 10000
            rval = self.pre_erun1(input_cnf, full_drup, adaptive=True, use_reduced_drup=True)
        else:
            self._report(0, "ERROR: invalid preprocessing value %s" % self.pre)
            return False
        if not rval: 
            self._report(0, "ERROR: unable to produce DRUP trace")
            return False

        # get the core
        self._report(1, "computing the core of %s using %s" % (input_cnf, full_drup))
        rcode = self._trim(input_cnf, full_drup, core_cnf, output_trace=full_trace)
        if rcode != 0:
            self._report(0, "ERROR: got %d from drup-trim tyring to computing the core" % rcode)
            return False
        self.stats.core_size = count_clauses(core_cnf)
        self._report(1, "the core is in %s" % core_cnf)
        self._report(1, "clauses in the core: %d" % self.stats.core_size)
        self._report(1, "full DRUP trace is in %s" % full_drup)
        if full_trace is not None:
            self._report(1, "full TRACECHECK trace is in %s" % full_trace)
        return True

    def trim(self, input_cnf, core_cnf, full_drup, full_trace=None):
        '''Trim at least once, or even more, if needed'''
        if self.pre not in ["erun4", "erun41", "erun5", "erun6"]:
            return self.single_trim(input_cnf, core_cnf, full_drup, full_trace=full_trace)
        
        def terminate(pre, prev_size, curr_size):
            if pre == "erun4":
                return (prev_size - curr_size) < 0.03 * prev_size
            if pre == "erun41":
                return (prev_size - curr_size) < 0.1 * prev_size
            if pre == "erun5":
                return (prev_size - curr_size) < 100
            if pre == "erun6":
                return prev_size == curr_size
            return True
        # adaptive loop of single_trims
        work_cnf = os.path.join(self.workdir_path, "work_main.cnf")
        shutil.copy(input_cnf, work_cnf)
        self._report(2, "initialized %s with %s" % (work_cnf, input_cnf))
        prev_size = sys.maxint
        curr_size = self.stats.input_size
        self._report(1, "starting outer loop, input size = %d" % curr_size)
        while not terminate(self.pre, prev_size, curr_size):
            self._report(2, "starting new iteration of the outer loop")
            osh.rm(full_drup) # make sure its clean
            if not self.single_trim(work_cnf, core_cnf, full_drup, full_trace=full_trace):
                return False
            prev_size = curr_size
            curr_size = self.stats.core_size
            self._report(2, "core sizes: previous=%d, new=%d" % (prev_size, curr_size))
            shutil.copy(core_cnf, work_cnf)
            self._report(2, "initialized %s with %s" % (work_cnf, core_cnf))

        self._report(1, "outer loop is terminated")
        self._report(1, "the core is in %s" % core_cnf)
        self._report(1, "clauses in the core: %d" % self.stats.core_size)
        return True
        
    def run(self, niters=1):
        '''Top-level entry point to run the framework.
        @param niters -- the number of iterations of the outer loop; 0 = unlimited
        @return -1 on error, 20 if MUS is computed, 0 if an overapproximation
        is computed.
        '''
        mc = (self.compute_mus_core + self.compute_mus_pc + 
              self.compute_mus_core_hmuc + self.compute_mus_trace)
        assert mc <= 1, "only one of the MUS computation methods is supported at a time"
        assert mc == 1 or self.outputcnf_path is None, "MUS computation is not requested, but output is"
                
        self._setup()

        if self.corecnf_path is None: # need the core always
            self.corecnf_path = os.path.join(self.workdir_path, "core.cnf")

        if (self.compute_mus_pc or self.compute_mus_trace or self.fullpc_path is not None):
            if self.fulltrace_path is None:
                self.fulltrace_path = os.path.join(self.workdir_path, "full.trace")

        if self.outputcnf_path is None:
            self.outputcnf_path = os.path.join(self.workdir_path, "mus.cnf")

        rcode = -1    # return value
        niter = 1     # current iteration number

        # dynamic params
        if self.adapt_time_lim:
            orig_call_time_lim = self.call_time_lim
            orig_mus_time_lim = self.mus_time_lim
        if self.super_mode: 
            orig_pre = self.pre
            orig_call_time_lim = self.call_time_lim
            self.call_time_lim = 0 
            self.pre = "none"
        
        while niters == 0 or niter <= niters:
        
            self._report(1, "starting iteration %d" % niter)

            if self.adapt_time_lim:
                if self.call_time_lim != 0: # to jive with super mode
                    self.call_time_lim = orig_call_time_lim * niter                    
                if self.mus_time_lim != 0:
                    self.mus_time_lim = orig_mus_time_lim * niter

            self._report(2, "controls: pre=%s, mtime=%s, ctime=%s" % (self.pre, self.mus_time_lim, self.call_time_lim));
            
            self.stats.input_size = count_clauses(self.inputcnf_path)
            self._report(1, "input clauses: %d" % self.stats.input_size)
            
            # get the core and maybe the trace
            if not self.trim(self.inputcnf_path, self.corecnf_path, 
                             self.fulldrup_path, self.fulltrace_path):
                self._report(0, "ERROR: failed to compute the core")
                return -1

            # compute pc file if needed
            if self.compute_mus_pc or self.fullpc_path is not None:
                assert self.fulltrace_path is not None, "should have .trace file here"
                if self.fullpc_path is None:
                    self.fullpc_path = os.path.join(self.workdir_path, "full.pc")
                self._report(1, "computing the .pc file from %s" % self.fulltrace_path)
                rcode = self._compute_pc(self.fulltrace_path, self.fullpc_path)
                if rcode != 0:
                    osh.print_status("dmuser", "ERROR: got %d while computing trace" % rcode)
                    return -1
                self._report(1, ".pc file is stored in %s" % self.fullpc_path)
            
            # get the list of IDs of necessary clauses, if known
            nec_ids = self._get_neccessary_clause_ids()
            if nec_ids is not None:
                assert len(nec_ids) == len(self.nec_ids), "the trimmed core must contain *all* necessary clauses !"
                if self.nec_ids_path is None:
                    self.nec_ids_path = os.path.join(self.workdir_path, "nec_ids.list")
                # write out the file
                nf = open(self.nec_ids_path, "w")
                for idx in nec_ids: print(str(idx), file=nf, end=" ")
                print("0", file=nf)
                nf.close()
            
            # compute MUS of the core with muser, if asked for
            if self.compute_mus_core:
                self._report(1, "computing an MUS of %s" % self.corecnf_path)
                rcode = self._compute_mus(self.corecnf_path, use_muser=True, 
                                          to=self.mus_time_lim, 
                                          nid_file=self.nec_ids_path, 
                                          call_to=self.call_time_lim)
                if rcode != 20 and rcode != 0:
                    self._report(0, "ERROR: got %d from muser-2" % rcode)
                    return -1
                self._report(1, "finished computation of an %sMUS of %s" % 
                                ("overapproximation of an " if rcode == 0 else "", self.corecnf_path))
            # compute MUS of the core with haifa_muc, if asked for
            elif self.compute_mus_core_hmuc:
                self._report(1, "computing an MUS of %s" % self.corecnf_path)
                rcode = self._compute_mus(self.corecnf_path, use_muser=False, 
                                          to=self.mus_time_lim, 
                                          nid_file=self.nec_ids_path, 
                                          call_to=self.call_time_lim)
                if rcode != 20 and rcode != 0:
                    self._report(0, "ERROR: got %d from haifa_muc" % rcode)
                    return -1
                self._report(1, "finished computation of an %sMUS of %s" % 
                                ("overapproximation of an " if rcode == 0 else "", self.corecnf_path))
            # compute MUS of the trace with haifa_muc_trace, if asked for
            elif self.compute_mus_trace:
                assert self.fulltrace_path is not None, "should have .trace file here"
                self._report(1, "computing an MUS of %s" % self.fulltrace_path)
                rcode = self._compute_mus(self.fulltrace_path, use_muser=False, 
                                          input_is_trace=True, 
                                          to=self.mus_time_lim, 
                                          nid_file=self.nec_ids_path,
                                          call_to=self.call_time_lim)
                if rcode != 20 and rcode != 0:
                    self._report(0, "ERROR: got %d from haifa_muc_trace" % rcode)
                    return -1
                self._report(1, "finished computation of an %sMUS of %s" % 
                                ("overapproximation of an " if rcode == 0 else "", self.corecnf_path))
            # compute pc file if needed and an MUS from it
            elif self.compute_mus_pc:
                assert self.fullpc_path is not None, "should have .pc file here"
                self._report(1, "computing an MUS of %s" % self.fullpc_path)
                rcode = self._compute_mus(self.fullpc_path, input_is_trace=True, 
                                          to=self.mus_time_lim, 
                                          nid_file=self.nec_ids_path,
                                          call_to=self.call_time_lim)
                if rcode != 20 and rcode != 0:
                    self._report(0, "ERROR: got %d from muser-2" % rcode)
                    return -1
                self._report(1, "finished computation of an %sMUS of %s" % 
                                ("overapproximation of an " if rcode == 0 else "", self.corecnf_path))
    
            # all done, write output file
            assert rcode == 0 or rcode == 20, "rcode should be 0 or 20 by now"
            if self._write_output(self.outputcnf_path):
                self._report(1, "wrote the %sMUS to %s" % 
                             ("overapproximation of an " if rcode == 0 else "", self.outputcnf_path))
            else:
                self._report(0, "ERROR: unable to write output file %s" % self.outputcnf_path)
                return -1

            # stats 
            self.stats.num_iter = niter
            self.stats.update_accs()  
            if not self.no_stats and self.verbosity >= 1: 
                self.stats.print_iter()
                self.stats.print_accs()
            self.stats.init_iter()

            if rcode == 20: break # done

            niter +=1
            self.inputcnf_path = self.outputcnf_path
            if self.pre_once: self.pre = "none"
            if self.super_mode:
                self.pre = orig_pre
                self.mus_time_lim = 0
                self.call_time_lim = orig_call_time_lim 
                
        return rcode

    def print_stats(self):
        self.stats.print_accs()
        
# Entry point for external callers
def main(argv):
    '''@return -1 on error, 0 if MUS overapproximation is computed, 20 if MUS
    is computed exactly'''
    def parseArgs(argv):
        p = argparse.ArgumentParser(description="DRUP-based MUS extractor",
                                    formatter_class=argparse.ArgumentDefaultsHelpFormatter)
        p.add_argument("input_file", type=str,
                       help="input CNF formula")
        p.add_argument("-o", "--output-file", type=str,
                       help="path to the file to store (overapprox.) of MUS", 
                       default=None)
        p.add_argument("-v", "--verb", type=int, 
                       help="verbosity level (0,1,2,3)", default=1)
        p.add_argument("-mi", "--main-iters", type=int,
                       help="maximum number of iterations of the main loop: the "
                       "loop is terminated when this number is exceeded or when "
                       "an exact MUS is computed. 0 means no limit.", default=1)
        p.add_argument("-pre", "--pre", type=str,
                       help="preprocessing flow: none, erun1 (-i iterations of the "
                       "preprocessor loop), erun3 (like erun1, but adaptive based on "
                       "the number of conflicts), erun4 (multi-erun4, adaptive, based "
                       "on core size reduction (3%%)), erun5 (like erun4, but absolute "
                       "reduction of 100 clauses), erun6 (like erun4, but till 0)", default="erun4")
        p.add_argument("-preonce", "--preonce",
                       help="with (-mi != 1): turn off preprocessing after the first iteration of the main loop",
                       action="store_true", default=False)
        p.add_argument("-super", "--super-mode",
                       help="with (-mi != 1); spend -mtime seconds on the first iteration "
                       "with -pre=none; then run -pre, set -mtime=0, and run the " 
                       "following iterations using -ctime",
                       action="store_true", default=False)
        p.add_argument("-i", "--iters", type=int,
                       help="maximum number of inner VE loop iterations when the inner loop is not dynamic", default=5)
        p.add_argument("-g", "--grow", type=str,
                       help="grow for VE as a function of the inner iteration number "
                       "a in [0, --iters), e.g. '10*(2**a)'", default="10*(2**a)")
        p.add_argument("-d", "--drup-file", type=str, 
                       help="if specified, the DRUP file used to obtain the core "
                            "will be stored in this file", default=None)
        p.add_argument("-c", "--core-file", type=str, 
                       help="if specified, the CNF core will be stored in this file", default=None)
        p.add_argument("-t", "--trace-file", type=str, 
                       help="if specified with, the TRACECHECK version of the "
                            "DRUP file will be stored in this file", default=None)
        p.add_argument("-p", "--pc-file", type=str, 
                       help="if specified, the .pc file (input to muser-2) will be "
                            "stored in this file", default=None)
        p.add_argument("-mc", "--compute-mus-core", 
                       help="compute MUS from the core using muser-2 (requires tools/muser-2)", 
                       action='store_true', default=False)
        p.add_argument("-mch", "--compute-mus-core-hmuc", 
                       help="compute MUS from the core using Haifa-MUC (requires tools/haifa_muc)", 
                       action='store_true', default=False)
        p.add_argument("-mp", "--compute-mus-pc", 
                       help="compute MUS from the resolution proof in .pc format "
                            "using muser-2 (requires tools/muser-2)", 
                       action='store_true', default=False)
        p.add_argument("-mt", "--compute-mus-trace", 
                       help="compute MUS from the resolution proof in TRACECHECK format "
                            "using Haifa-MUC (requires tools/haifa_muc_trace)", 
                       action='store_true', default=False)
        p.add_argument("-mtime", "--mus-time-lim", type=int,
                       help = "time limit in seconds for the MUS extractor, 0 = no limit",
                       default=0)
        p.add_argument("-ctime", "--call-time-lim", type=int,
                       help = "time limit in seconds for a single SAT call of the MUS extractor, 0 = no limit",
                       default=0)
        p.add_argument("-atime", "--adapt-time-lim",
                       help = "make -mtime(-ctime) to be -mtime(-ctime)*num_iter",
                       action = "store_true", default=False)
        p.add_argument ("--tools-path", type=str, 
                        help="the path to the tools directory",
                        default=os.path.abspath(os.path.dirname(__file__)) + "/tools")
        p.add_argument ("--workdir-path", type=str, 
                        help="the path to the working directory to store files; "
                             "if not specified, a directory under /tmp is used",
                        default=None)
        p.add_argument("-k", "--keep-files", 
                       help="keep all temporary files", 
                       action='store_true', default=False)
        p.add_argument("-ns", "--no-stats", 
                       help="do not print stats at the end of the execution", 
                       action='store_true', default=False)
        p.add_argument("-m2:prog", "--muser2:prog", dest="muser2_use_prog",
                       help="muser-2: use progression algorithm instead of default", 
                       action='store_true', default=False)
        return p.parse_args (argv)

    def signal_handler(signal, frame):
        osh.print_status("dmuser", "caught signal %r (time might be wrong), terminating." % signal)
        pass
    # go ...
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)
    args = parseArgs(argv[1:])
    if args.verb:
        osh.print_status("dmuser", "arguments: %s" % reduce(lambda x,y: x + " " + y, argv[1:]))
    dm = DMuser()
    rcode = -1 # exit code
    try:
        dm.inputcnf_path = args.input_file
        dm.outputcnf_path = args.output_file
        dm.verbosity = args.verb
        dm.pre = args.pre
        dm.pre_once = args.preonce
        dm.super_mode = args.super_mode
        dm.ve_iters = args.iters
        dm.ve_grow = args.grow
        dm.corecnf_path = args.core_file
        dm.fulldrup_path = args.drup_file
        dm.fulltrace_path = args.trace_file
        dm.fullpc_path = args.pc_file
        dm.compute_mus_core = args.compute_mus_core
        dm.compute_mus_core_hmuc = args.compute_mus_core_hmuc
        dm.compute_mus_trace = args.compute_mus_trace
        dm.compute_mus_pc = args.compute_mus_pc
        dm.tools_path = args.tools_path
        dm.workdir_path = args.workdir_path
        dm.keep_files = args.keep_files
        dm.mus_time_lim = args.mus_time_lim
        dm.call_time_lim = args.call_time_lim
        dm.adapt_time_lim = args.adapt_time_lim        
        dm.muser2_use_prog = args.muser2_use_prog
        dm.no_stats = args.no_stats
        # sanity checks ...
        s = dm.compute_mus_core + dm.compute_mus_pc + dm.compute_mus_core_hmuc + dm.compute_mus_trace
        if s != 1: 
            raise ValueError("exactly one of -mc, -mch, -mt, -mp must be specified")
        if args.main_iters == 1 and (dm.pre_once or dm.super_mode):
            raise ValueError("-preonce or -super make no sence with -mi=1")
        if dm.pre_once and dm.super_mode: 
            raise ValueError("-preonce and -super are mutually exclusive")
        if dm.super_mode and (dm.mus_time_lim == 0 or dm.call_time_lim == 0):
            raise ValueError("both -mtime and -ctime should be provided with -super")
        if dm.adapt_time_lim and (dm.mus_time_lim == 0 and dm.call_time_lim == 0):
            raise ValueError("-atime makes no sense without -mtime or -ctime")
        rcode = dm.run(args.main_iters)
    except IOError as e:
        osh.print_status("dmuser", "ERROR: %r on %s" % (e, e.filename))
    except Exception as e:
        if (e.message): osh.print_status("dmuser", "ERROR: " + e.message)
        else: osh.print_status("dmuser", "ERROR: %r " % e)
    finally:
        return rcode

if __name__ == "__main__":
    sys.exit(main(sys.argv))
    
