'''
A little module to keep all the useful OS-related funcitonality.

Created on Nov 27, 2013

@author: Anton Belov; many of functions are borrowed from ufo.py by Arie Gurfinkel
'''

import atexit
import os
import re
import tempfile
import shutil
import subprocess
import sys

def isexec(fpath):
    '''Check if fpath refers to an executable'''
    if fpath == None: return False
    return os.path.isfile(fpath) and os.access(fpath, os.X_OK) 
    
def rm(path):
    if path is not None and os.path.exists(path):
        os.remove(path)
    
def getToolPath(root, tool):
    tp = os.path.join (root, tool)
    if not isexec(tp): raise IOError("cannot find executable " + tp)
    return tp

def createWorkDir(dname = None, save = False):
    '''Create a working directory; if save = False, the directory removed on exit
    @return path to the working directory
    '''    
    if dname == None:
        dname = tempfile.mkdtemp()
    else:
        if os.path.exists(dname): 
            shutil.rmtree(dname, ignore_errors=True)
        os.makedirs(dname)
    if not save: atexit.register(shutil.rmtree, path=dname)
    return dname


def start_command(command):
    '''Kicks off the execution of command; returns a pair (iter, p) where iter
    is the iterator to the stdout of the command (with stderr piped into stdout),
    and p is a process handle to be passed into finish_command(). Typical usage:
        c = start_command("....")
        for line in c[0]:
            # do something with the line
        exit_code = finish_command(c[1])
    '''
    p = subprocess.Popen(command.split(), stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    return (iter(p.stdout.readline, ''), p)

def finish_command(p):
    '''Waits for command to finish (see start_command)
    @return the exit code of the command
    '''
    p.wait()
    return p.returncode

def run_command(command, out=sys.stdout):
    '''Runs the command and waits for its completion; pipes stdout and stderr
    into the 'out'. If out=None, stdout is piped into /dev/null
    @return exit code of the command
    '''
    p = subprocess.Popen(command.split(), 
                         stdout=(open(os.devnull, 'w') if out is None else out),
                         stderr=subprocess.STDOUT)
    p.wait()
    return p.returncode

def get_cpu_time():
    '''Returns the total current CPU time (user+sys) of this process and its 
    immediate children (non-recursive, i.e. children of children are not included)'''
    return sum(os.times()[:4])

def print_status(tool, status):
    print "[%s @ %.2f sec] %s" % (tool, get_cpu_time(), status)

def cat(dest, sources, append=False):
    '''Cats sources into the destination file; if append=True, dest is opened 
    with a'''
    outf = open(dest, 'a' if append else 'w')
    for src in sources:
        shutil.copyfileobj(open(src, 'r'), outf)
    outf.close()
    
def wc(filename, opt='l'):
    '''Returns the number of lines/bytes in the file (wc -l ~5x faster, TODO: fix)'''
    (f, res) = (open(filename), -1)
    if opt == 'l':
        for i,_ in enumerate(f): pass
        res = i + 1
    f.close()
    return res

def sort(file_in, file_out=None, num=False):
    '''Sorts file file_in, and writes it out to file_out, or back to file_in
    if file_out=None (~10x faster than UNIX sort); if num=True sort is 
    numeric (using UNIX sort -n)'''
    if num: # todo - replace with sort -n
        if file_out is None:
            file_out_r = tempfile.mkstemp(text=True)[1]
        else:
            file_out_r = file_out
        run_command("sort -n " + file_in, out=open(file_out_r, "w"))
        if file_out is None: shutil.move(file_out_r, file_in)
    else: 
        s = sorted(open(file_in))
        if file_out is None: file_out = file_in
        open(file_out, "w").writelines(s)

# test code
if __name__ == "__main__":
    #sort(sys.argv[1], sys.argv[2], num=True)
    sort(sys.argv[1], None, num=True)
    #print wc(sys.argv[1])